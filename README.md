# Android Record Dialog [![](https://jitpack.io/v/com.gitlab.massarttech/android-record-dialog.svg)](https://jitpack.io/#com.gitlab.massarttech/android-record-dialog)
**Android file picker.**

### Setup
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:
```
dependencies {
    implementation 'com.gitlab.massarttech:android-record-dialog:1.0.1'
}
```

**Credits https://github.com/IvanSotelo/RecordDialog**